ARG IMAGE_TAG=latest
FROM docker.io/python:${IMAGE_TAG}

COPY requirements.txt .

ENV PIP_ROOT_USER_ACTION=ignore
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

ENV PATH="/root/.cargo/bin:${PATH}"

ARG DEBIAN_FRONTEND=noninteractive
ARG PIP_CMD="pip install --no-cache-dir --upgrade -r requirements.txt"

RUN . /etc/os-release && \
    if [ "${ID}" = "alpine" ]; then \
    apk upgrade && apk add --no-cache curl build-base libffi-dev rust cargo && ${PIP_CMD} && apk del build-base libffi-dev rust cargo; \
    elif [ "${ID}" = "debian" ]; then \
    apt-get update && apt-get full-upgrade -y && apt-get install -y curl build-essential && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y && ${PIP_CMD} && rustup self uninstall -y && apt-get purge -y build-essential && apt-get autopurge -y;\
    else \
    ${PIP_CMD}; \
    fi
